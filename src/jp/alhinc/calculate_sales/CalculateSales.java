package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String INVALID_BRANCH_CODE = "の支店コードが不正です";
	private static final String BRANCH_REGEX = "^[0-9]{3}$";
	private static final String SALEFILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String AMOUNT_TOTAL_OVER_10TH = "合計金額が10桁を超えました";
	private static final String INVALID_FORMAT = "のフォーマットが不正です";
	private static final String INVALID_COMMODITY_CODE = "の商品コードが不正です";
	private static final String COMMODITY_REGEX = "^[A-Za-z0-9]{8}$";
	private static final String BRANCH_DEFINISION = "支店定義";
	private static final String COMMODITY_DEFINISION = "商品定義";
	private static final String FILE_NOT_EXIST = "ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "ファイルのフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		Map<String, String> branchCode = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		Map<String, String> commodityCode = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();

		//コマンドライン引数が渡されてない場合のエラー処理(エラー処理3-1)
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchCode, branchSales, BRANCH_DEFINISION, BRANCH_REGEX)) {
			return;
		}
		// 商品定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityCode, commoditySales,COMMODITY_DEFINISION, COMMODITY_REGEX)) {
			return;
		}

		//rcdファイルの集計
		File[] salesFile = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < salesFile.length; i++) {
			String fileNames = salesFile[i].getName();
			//読み込んだものがファイルなのかディレクトリなのか判定する処理を実装（補足資料09)
			if(salesFile[i].isFile() && fileNames.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(salesFile[i]);
			}
		}
		//売上ファイルリストのソート
		Collections.sort(rcdFiles);
		//売上ファイルの名前が連番になってない場合のエラー処理（エラー処理2-1)
		for(int i = 0; i < rcdFiles.size() - 1 ;i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println(SALEFILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		//rcdファイルの読込
		BufferedReader br = null;
		for (int i = 0; i < rcdFiles.size(); i++) {
			File rcdFileName = rcdFiles.get(i);
			try {
				FileReader fr = new FileReader(rcdFileName);
				br = new BufferedReader(fr);
				ArrayList<String> readLineList = new ArrayList<>();
				String readFile;
				while((readFile = br.readLine()) != null) {
					readLineList.add(readFile);
				}
				String fileNames = rcdFiles.get(i).getName();
				//売上ファイルの中身が3行ではなかった場合のエラー処理（エラー処理2-4）
				if(readLineList.size() != 3) {
					System.out.println(fileNames + INVALID_FORMAT);
					return;
				}
				//売上ファイルの売上金額が数字ではなかった場合のエラー処理(エラー処理3-2)
				if(!readLineList.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				//売上ファイルの支店コードが支店定義ファイルに該当しない場合のエラー処理（エラー処理2-3）
				if(!branchCode.containsKey(readLineList.get(0))){
					System.out.println(fileNames + INVALID_BRANCH_CODE);
					return;
				}
				//売上ファイルの商品コードが商品定義ファイルに該当しない場合のエラー処理（エラー処理2-4)
				if(!commodityCode.containsKey(readLineList.get(1))) {
					System.out.println(fileNames + INVALID_COMMODITY_CODE);
					return;
				}
				//売上金額をlong型に変換
				long fileSale = Long.parseLong(readLineList.get(2));
				//売上集計をMapに格納
				Long saleAmount = branchSales.get(readLineList.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(readLineList.get(1)) + fileSale;
				//売上集計で合計金額が10桁を超えた場合のエラー処理（エラー処理2-2）
				if((saleAmount >= 10000000000L) || (commoditySaleAmount >= 10000000000L)) {
					System.out.println(AMOUNT_TOTAL_OVER_10TH);
					return;
				}
				branchSales.put(readLineList.get(0), saleAmount);
				commoditySales.put(readLineList.get(1), commoditySaleAmount);
			}catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchCode, branchSales)) {
			return;
		}
		// 商品別集計ファイルの書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityCode, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル、商品定義ファイルの読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> code, Map<String, Long> sales,String definision, String regex) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			//支店定義ファイル、商品定義ファイルの読込に関するエラー処理(エラー処理1-1、1-3）
			if(!file.exists()) {
				System.out.println(definision + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] splitNames = line.split(",");
				//支店定義ファイル、商品定義ファイルのフォーマットに関するエラー処理（エラー処理1-2,1-4）
				if((splitNames.length != 2) || (!splitNames[0].matches(regex))) {
					System.out.println(definision + FILE_INVALID_FORMAT);
					return false;
				}
				code.put(splitNames[0], splitNames[1]);
				sales.put(splitNames[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> code, Map<String, Long> sales) {


		Map<String, Long> result = sales.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey,  Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));

		BufferedWriter bw = null;
		try {
			File file = new File(path,fileName);
			bw = new BufferedWriter(new FileWriter(file));
			for(String key : result.keySet()) {
				bw.write(key + "," + code.get(key) + "," + result.get(key));
				bw.newLine();
			}
		}catch(IOException e){
			System.out.println(UNKNOWN_ERROR);
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
