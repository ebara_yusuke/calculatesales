package stream.api;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CalculateSales {

	public static void main(String[] args){

		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();
		//�x�X�R�[�h�̐��K�\���p
		String regex = "^[0-9]{3}$";

		if ( !lstFileRead(args[0], "branch.lst", regex, "支店", branchNames, branchSales) ) {
			return;
		}

		regex = "^[0-9A-Za-z]{8}$";

		if ( !lstFileRead(args[0], "commodity.lst", regex, "商品", commodityNames, commoditySales) ) {
			return;
		}


		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for ( int i = 0; i < files.length ; i++ ) {

			String fileName = files[i].getName();
			if( files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		if (rcdFiles.size() >= 2) {
			Collections.sort(rcdFiles);
			int fileNameMin = Integer.parseInt(rcdFiles.get(0).getName().substring(0,8));
			int fileNameMax = Integer.parseInt(rcdFiles.get(rcdFiles.size() - 1).getName().substring(0,8));
			if( (fileNameMax - fileNameMin + 1) != rcdFiles.size()) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		BufferedReader br = null;
		for (int i = 0; i< rcdFiles.size(); i++) {
			try {
				ArrayList<String> list = new ArrayList<>();
				String fileName = rcdFiles.get(i).getName();
				String line = "";
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));

				int branchCodeLine = 0;
				int commodityCodeLine = 1;
				int saleLine = 2;
				int lineSize = 3;
				long saleMax = 10000000000L;
				while ((line = br.readLine()) != null) {
					list.add(line);
				}

				if (list.size() != lineSize) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}

				if (!list.get(saleLine).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				if (!branchNames.containsKey(list.get(branchCodeLine))) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				if (!commodityNames.containsKey(list.get(commodityCodeLine))) {
					System.out.println(fileName + "の商品コードが不正です");
					return;
				}
				String branchCode = list.get(branchCodeLine);
				String commodityCode = list.get(commodityCodeLine);

				long fileSale = Long.parseLong(list.get(saleLine));
				Long saleAmount = branchSales.get(branchCode) + fileSale;
				if(saleAmount >= saleMax){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(branchCode, saleAmount);
				// ���i�����Z
				saleAmount = commoditySales.get(list.get(commodityCodeLine)) + fileSale;
				if(saleAmount >= saleMax){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				commoditySales.put(commodityCode, saleAmount);

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}


		if (!outFileWrite(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

		if (!outFileWrite(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}



	}
	/**
	 *
	 * @param dirPath �f�B���N�g���p�X
	 * @param fileName �t�@�C������
	 * @param regex ���K�\��
	 * @param type  �G���[���b�Z�[�W�ŏo�͂���Ƃ��̖���
	 * @param names  �R�[�h�A���O�̃}�b�v
	 * @param sales�@�R�[�h�A���z�̃}�b�v
	 * @return
	 */
	public static boolean lstFileRead(String dirPath, String fileName, String regex, String type, Map<String, String> names, Map<String, Long> sales) {
		BufferedReader br = null;
		try{
			File file = new File(dirPath, fileName);
			if(!file.exists()) {
				System.out.println(type + "定義ファイルが存在しません");
				return false;
			}
			br = new BufferedReader(new FileReader(file));
			String line;
			while((line = br.readLine()) != null){
				String[] items = line.split(",", -1);
				if((items.length != 2) || (!items[0].matches(regex))){
					System.out.println(type + "定義ファイルのフォーマットが不正です");
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if( br != null ){
					br.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
	/**
	 * @param dirPath
	 * @param fileName
	 * @param names
	 * @param sales
	 * @return
	 */
	public static boolean outFileWrite(String dirPath, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;

		Map<String, Long> result = sales.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
				(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		try {
			File file = new File(dirPath, fileName);
			bw = new BufferedWriter( new FileWriter(file) );
			for (String key : result.keySet()) {
				bw.write(key + "," + result.get(key) + "," + result.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if( bw != null ){
					bw.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}

		}
		return true;
	}

}
